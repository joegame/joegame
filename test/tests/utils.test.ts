import * as Phaser from 'phaser'
import { getVolAndPanFromDistance } from 'joegamelib/src/utils/getVolPanFromDist'
import loadAfterLoad from 'joegamelib/src/utils/loadAfterLoad'
import { hashToArr } from 'joegamelib/src/utils/hashToArr'
import { syllableCount } from 'joegamelib/src/utils/syllableCount'
import { getTestScene } from '../testutils/test-scene-config'
import sinon from 'sinon'
import {expect} from "./imports"

describe('hashToArr function', function() {
    it('returns correct number of positive single digit integers', function() {
        ['test', 'howdy.', 'whasakjndsa221', 'aksjdq01j*wkj/', '2198123&&^2712131<>1928'].forEach(str => {

            const result = hashToArr(str, 3)
            expect(result).to.have.lengthOf(3)
            expect(result[0]).to.be.a('number').and.to.not.be.below(0)
            expect(result[1]).to.be.a('number').and.to.not.be.below(0)
            expect(result[2]).to.be.a('number').and.to.not.be.below(0)

            expect(result[0]).to.not.be.above(9)
            expect(result[1]).to.not.be.above(9)
            expect(result[2]).to.not.be.above(9)
        })
        // expect(result).to.not.include.a
    })
    it('will always be able to return twice the amount of numbers as syllables found', function() {
        ['capitol', 'personality', 'parlimentarian', 'humanitarian', 'richardsonsteinstipple'].forEach(item => {
            const syll = syllableCount(item)
            const hash = hashToArr(item, syll * 2)
            expect(hash).to.have.lengthOf(syll * 2)
        })
    })
})

describe('get vol and pan from distance', function() {
    it('will return a tuple with reasonable vol and pan modifiers based on a distance', function() {
        const first = getVolAndPanFromDistance(150, 150, 0, 0, 800)
        console.log(getVolAndPanFromDistance(15000, 150, 0, 0, 800))
        console.log(getVolAndPanFromDistance(150, 150, 150, 150, 800))
        expect(first[0]).to.be.greaterThan(0)
    })
})

describe('loadAfterLoad function', function(): void {
    let scene: Phaser.Scene, loaded: string, spy: sinon.SinonSpy
    beforeEach(async function(): Promise<void> {

        scene = await getTestScene()
        // const scene = game.scene.add('test', {})
        loaded = await loadAfterLoad(scene, 'test-loadAfterload', 'assets/images/mont-sainte-victoire.jpg', 'image')
    })
    it('loads file as a promise that returns the key from the loader', async function() {
        expect(loaded).to.be.an('string')
        expect(scene.textures.exists('test-loadAfterload')).to.be.true
        // expect(scene.load.listenerCount('filecomplete')).to.eq(0)
        scene.game.destroy(true)
    })
    it('does not try to download something thats already been downloaded, just returns key', async function() {
        spy = sinon.spy(scene.load, 'start')
        await loadAfterLoad(scene, 'test-loadAfterload', 'assets/images/mont-sainte-victoire.jpg', 'image')
        expect(spy.callCount).to.eq(0)
        await loadAfterLoad(scene, 'test-loadAfterload', 'assets/images/mont-sainte-victoire.jpg', 'image')
        expect(spy.callCount).to.eq(0)
        await loadAfterLoad(scene, 'test-loadAfterload', 'assets/images/mont-sainte-victoire.jpg', 'image')
        expect(spy.callCount).to.eq(0)
        // expect(true).to.be.true

    })

    it('can be run in parallel ok', async function() {
        await Promise.all([
            loadAfterLoad(scene, 'test-loadAfterload', 'assets/images/mont-sainte-victoire.jpg', 'image'),
            loadAfterLoad(scene, 'test-loadAfterload2', 'assets/images/toilet.png', 'image'),
            // loadAfterLoad(scene, 'test-loadAfterload3', 'assets/images/toilett.jpg', 'image'),
        ])
        expect(scene.textures.exists('test-loadAfterload')).to.be.true
        expect(scene.textures.exists('test-loadAfterload2')).to.be.true
        expect(scene.textures.exists('test-loadAfterload3')).to.be.false
    })
    it.skip('can fail gracefully', async function() {
        try {
            await loadAfterLoad(scene, 'test-loadAfterload2', 'assets/images/toilet.jpeggggg', 'image')
        } catch (err) {
            // Do something

        }
        expect().to.throw
    })
    afterEach(function() {
        scene.game.destroy(true)
    })
})
