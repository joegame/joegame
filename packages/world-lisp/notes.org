:PROPERTIES:
:ID:       8acb4b22-b121-4bec-a109-1286b8380d26
:END:
#+title: joegame world schema
* Main Concepts
In [[id:b8f0a328-ebd7-4142-86f3-a278e6d5b6de][joegame]], the overall structure of the world is divided into certain main concepts.
** area
The toplevel world of joegame is a [[id:f7daa8ab-3c30-4a8a-9065-6fc16af4c9c7][signal]], or collection of [[id:aff02f4b-c722-43cc-a3fa-60ac8f9ad2b5][merged signals]], which produce a list of areas for each coordinate, lowest depth to highest.

Every area has a name, a color to represent it in the world map, and its own signal, which produces a list of placed terrains given a coordinate, running from terr at the lowest depth to the highest.

Each area is also a 4x4 chunk of terrain /spots/.  They are not 4x4 chunks of terrains, because every spot is a list of terrains
** terrain
A terrain has a name, color for the map, and a set of particles and their likelihood.

A given absolute coordinate will have an associated list of terrains a "terrain spot", which represent terrains at low to high/ground-level depth.

*** querying
To get a certain terrain spot within an area spot, you ask for some division (0.00,0.25,0.50,0.75) is used like so for an absolute coordinate: get_terrain(3.2,100.2), get_terrain(2.00, 2000.25).

** tiles
A given terrain on a given depth level corresponds to 4x4 placed tiles, but for every such chunk, 4 terrains need to be queried for. Chunks are placed along each point where four terrains meet on a given level. Using the collective values of the four terrains a given quad is placed. Each terrain is treated separately, so the four terrains asked about are always either "terrain" or "not-terrain"
*** quad
A specific 4x4 arrangement of tiles, typically responding to a wang tile, and placed by checking terrains.

*** wang set
A wang set is a set of 16 quads that can be used as wang tiles.
*** tileset
An actual png file that has certain amount of tiles, used to make the quads of a wang set.
** particles
A terrain is composed of particles.  Particles have a color-range and a name.
** requesting a map
A map is 128x128 tiles, or 8x8 areas, where 1 area is 4 terrains, and each terrain is 4 tiles. 16 tiles per area.

The main function to retrieve a map takes simple indexes and multiplies by 128.

First a 8x8 grid of area lists are retrieved

* Areas
#+name: joegame-area
| name                  | color   |
|-----------------------+---------|
| depths                | #313e49 |
| trench                | #5c758a |
| ocean                 | #B7C4CF |
| shore                 | #e0b483 |
| late-shore            | #c69763 |
| coastal               | #c6ad74 |
| grass-and-sand        | #839450 |
| rocky-sand            | #B18E68 |
| desert                | #ffffd3 |
| desert-graveyard      | #faa06b |
| dead-forest           | #f4c992 |
| old-pavement-desert   | #b89a74 |
| boulder-meadow-desert | #96794d |
| water-desert          | #c5e9bd |
| field                 | #33590e |
| old-pavement-field    | #8f8f51 |
| forest                | #293b09 |
| forest-magic          | #2e4114 |
| water-forest          | #2e352e |
| old-pavement-forest   | #444353 |

* setup

#+begin_src lisp
(uiop:getcwd)
*gg*
#+end_src

#+RESULTS:
: 420
