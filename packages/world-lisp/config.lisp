(defpackage config (:use :cl)
  (:export
    *tiled-json-version*
    *tiled-version*))
(in-package config)
(defvar *tiled-json-version* "1.10")
(defvar *tiled-version* "1.10.1")
